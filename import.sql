CREATE TABLE `g1k_links` (
 `id` int(11) NOT NULL AUTO_INCREMENT,
 `url` varchar(512) COLLATE utf8_unicode_ci DEFAULT NULL,
 `shorturl` varbinary(32) DEFAULT NULL,
 `created` timestamp NOT NULL DEFAULT current_timestamp(),
 `count` int(11) NOT NULL DEFAULT 0,
 `unic_count` int(11) NOT NULL DEFAULT 0,
 `disabled` int(1) NOT NULL DEFAULT 0,
 `permission` int(1) NOT NULL DEFAULT 0,
 `password` varchar(128) COLLATE utf8_unicode_ci DEFAULT NULL,
 `ip` varchar(128) CHARACTER SET utf8 DEFAULT NULL,
 `user_agent` varchar(512) CHARACTER SET utf8 DEFAULT NULL,
 PRIMARY KEY (`id`),
 KEY `shorturl` (`shorturl`)
) ENGINE=InnoDB AUTO_INCREMENT=178 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci

CREATE TABLE `g1k_statistics` (
 `id` int(11) NOT NULL AUTO_INCREMENT,
 `link_id` int(11) NOT NULL,
 `ip` varchar(64) COLLATE utf8_unicode_ci NOT NULL,
 `referer` varchar(512) COLLATE utf8_unicode_ci DEFAULT NULL,
 `user_agent` varchar(512) COLLATE utf8_unicode_ci DEFAULT NULL,
 `time` timestamp NOT NULL DEFAULT current_timestamp(),
 PRIMARY KEY (`id`),
 KEY `link_id` (`link_id`)
) ENGINE=InnoDB AUTO_INCREMENT=29371 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci
